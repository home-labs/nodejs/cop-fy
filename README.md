

# Copfy

## dependencies

- @cyberjs.on/nodir
- @cyberjs.on/jazzon

## Installing

	$ npm i @cyberjs.on/copfy --save-dev

## Usage

Create a file path `@cyberjs.on/copfy.json` in your project's directory, with the below model settings:

```json
{
  "$schema": "../../node_modules/@cyberjs.on/copfy/schema.json",
  "rootDirectory": "../../",
  "directorynameOfTranspiledLibrary": "path/to/build/the/library/on",
  "settings": {
    "patterns4IgnorePaths": [
      ".git/...",
      "node_modules/...",
      "settings/...",
      "another-foldername/...",
    ],
    "copies": [
      {
        "fileExtensionPatterns": [
          "ext",
          "*.ext2.ext"
        ],
        "path": [
          {
            "patterns": [
              "src/lib/subfolder-1/subfolder-1.1/subfolder-1.1.1/..."
            ],
            "copy2": "../../../lib/subfolder-2",
          }
        ]
      }
    ]
  }
}
```

Create a `.npmrc` file with `cyberjs_on_settings_directory` environment variable at the folder when is the `package.json`, with the below content model

```npmrc
cyberjs_on_settings_directory = ${INIT_CWD}/settings
```

### A Suggestion Of Package.json File

```json
  "scripts": {
    "copfy:copy-files": "npm explore @cyberjs.on/copfy -- npm run copy-files",
    "build": "tsc -b && npm run copfy:copy-files",
  }
```

So, inside the root directory of your project (where the `package.json` is), run:

	$ npm run build

## Settings

|Name|Description|
|--|--|
|rootDirectory|Required.<br/>Type: string<br/>Relative to path of `copfy.json` file.
|directorynameOfTranspiledLibrary|Required.<br/>Type: string<br/>Relative to root directory of the library.
|settings|Required.<br/>Type: object
|ignoreByPathPatterns|Optional.<br/>Type: string[]
|copies|Required.<br/>Type: object[]
|fileExtensionPatterns|Optional.<br/>Type: string[]
|path|Required.<br/>Type: object[]<br/>Relative to path defined in `<rootDirectory>/<directorynameOfTranspiledLibrary>`
|patterns|Required.<br/>Type: string[]
|copy2|Optional.<br/>Type: string.<br/>Relative to path defined in `settings.copies.path`.
