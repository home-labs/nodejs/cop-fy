import NodeFs from 'node:fs';
import NodeUrl from 'node:url';
import NodePath from 'node:path';
import NodeChild_process from 'node:child_process';

import { Nodir } from '@cyberjs.on/nodir';
import { SyOfi } from '@cyberjs.on/syofi';


const currentPathname = NodePath.dirname(NodeUrl.fileURLToPath(import.meta.url));

const compiledRootDirectorynameOfProject = 'es';

Nodir.Path.operatingSystemPathSeparator = NodePath.sep;

const directorynameOfTests = Nodir.Path.join(
  Nodir.Path.normalize(currentPathname,
    {
      operatingSystemSeparator: NodePath.sep
    }
  ),
  compiledRootDirectorynameOfProject, 'src/tests'
);

const settingsFilePathname = Nodir.Path.join(directorynameOfTests, 'settings/@cyberjs.on/copfy.json');

const jsonHandle = new SyOfi.JsonFileHandler(settingsFilePathname);

const content = await jsonHandle.read();

const resolvedSchema = (new Nodir.Path(content['$schema'], '/')).withoutUpperFloors(1);

const npmrcFilename = '.npmrc';

const npmrcFileOfTestsPathname = Nodir.Path.join('./ts/src/tests', npmrcFilename);

const npmrcPathnameTargetPathname = Nodir.Path.join(directorynameOfTests, npmrcFilename);

console.log('\nCopying', `${npmrcFileOfTestsPathname}`, 'file to', directorynameOfTests);
console.log('------------\n');
NodeFs.copyFile(
  npmrcFileOfTestsPathname,
  npmrcPathnameTargetPathname,
  (error) => {

      if (error) {
        throw new Error(error.message);
      }

      console.log('\n', `${npmrcFileOfTestsPathname}`, 'was copied to', directorynameOfTests);
      console.log('------------\n');
  }
);

// relative path (from current path, that is the root path) when the package.json to run the scripts is
process.chdir(compiledRootDirectorynameOfProject);

// install the packages for tests
NodeChild_process.execSync('npm --prefix src/tests install tests');

console.log('\nInstalling node packages on', directorynameOfTests);
console.log('------------\n');

content['$schema'] = resolvedSchema;

console.log('\nUpdating', settingsFilePathname, 'file');
console.log('------------\n');

jsonHandle.write(content);
