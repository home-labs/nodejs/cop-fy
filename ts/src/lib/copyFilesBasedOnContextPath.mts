import NodeChild_process from 'node:child_process';
import NodeFs from 'node:fs';
import NodePath from 'node:path';

import { Path, Helpers as NodirHelper } from '@cyberjs.on/nodir';
import {
    DiMan,
    Directory
} from '@cyberjs.on/di-man';
import { JsonFileHandler } from '@cyberjs.on/syofi';


export async function copyFilesBasedOnContextPath(appendPathname?: string) {

    const copyFiles = (
        filenamePaths: Path[],
        fromPathname: string,
        toPathname: string
    ): void => {

        let filename: string | null;

        Directory.create(toPathname);

        for (let filenamePath of filenamePaths) {

            filename = filenamePath.basename;

            // debugger
            if (preCompiledPathnameSettings2Copy.filenames?.includes(filenamePath.toString())
                || (
                    canCopyingAccordingExtension(filename, fileExtensionPatterns)
                    && !pathData2Ignore?.filenames?.includes(filenamePath.toString())
                )
            ) {
                copyFile(
                    filename!,
                    fromPathname,
                    toPathname
                );
            }

        }

    }

    const copyFile = (
        filename: string,
        fromPathname: string,
        toPathname: string
    ): void => {

        let joinedFilenameFromPathname: string;

        let joinedFilename2Pathname: string;

        joinedFilenameFromPathname = Path.join(fromPathname, filename);
        joinedFilename2Pathname = Path.join(toPathname, filename);

        if (replaceIfAlreadyExists || !NodeFs.existsSync(joinedFilename2Pathname)) {

            if (verbose) {
                console.log(joinedFilenameFromPathname, 'was copied to', toPathname);
                console.log('------------\n');
            }

            NodeFs.copyFile(
                joinedFilenameFromPathname,
                joinedFilename2Pathname,
                (error: NodeJS.ErrnoException | null) => {

                    if (error) {
                        throw new Error(error.message);
                    }
                }
            );
        }


    };

    const canCopyingAccordingExtension = (
        filename: string | null,
        extensionPatterns: string[] = []
    ) => {

        const extension: string | null = new Path(filename || '').extension;

        let wildcardExtensions: string[];

        if (filename && extension) {
            wildcardExtensions = NodirHelper.File.collectWildCardExtensions(extensionPatterns);

            if ((wildcardExtensions.length
                && NodirHelper.File.extensionMatches2Wildcard(extension, wildcardExtensions))
                || extensionPatterns.includes(extension)
            ) {
                return true;
            }
        }

        return false;
    };

    const resolveCopy2Path = (
        pathname: string,
        copy2?: string
    ) => {

        let path = new Path(pathname);

        path = path.attachNewParent(compiledSourcePath.toString());

        if (copy2) {
            path = path.attachNew(copy2);
        }

        path.url = Path.relativeResolve(path.toString());

        return path;
    };

    const preCompilePathnameSettings2Copy = (pathSettings: object[]) => {

        const pathData: PathData = {
            filenames: [],
            shallowPathnames: []
        };

        let digestPathname: string | null;

        for (let pathSetting of pathSettings) {
            if (Object.hasOwn(pathSetting, 'patterns')) {

                (pathSetting as any).patterns.forEach(
                    (subpathname: string) => {

                        digestPathname = resolveAbsolutePathname(absoluteRootPathname, subpathname);

                        if (digestPathname) {
                            if (Directory.isDirectory(digestPathname)) {
                                if (!copyIsInDepth(subpathname)
                                    && !pathData2Ignore?.shallowPathnames?.includes(digestPathname)
                                    )
                                {
                                    pathData.shallowPathnames!.push(digestPathname);
                                }
                            } else {
                                if (!pathData2Ignore?.filenames?.includes(digestPathname)) {
                                    pathData.filenames!.push(digestPathname);
                                }
                            }
                        }
                    }
                );
            }
        }

        return pathData;
    };

    const copyIsInDepth = (pathname: string) => {
        return pathname.endsWith('...');
    };

    const resolveCurrentPathPatterns2Copy = (pathnames: string[]) => {

        const data: PathData = {
            pathnamesInDepth: [],
            shallowPathnames: [],
            filenames: []
        };

        let resolvedAbsolutePathname: string | null;

        let digestPathname: string;

        let path: Path;

        for (let pathname of pathnames) {

            path = new Path(pathname);

            digestPathname = path.toString();

            resolvedAbsolutePathname = resolveAbsolutePathname(absoluteRootPathname, pathname);

            if (resolvedAbsolutePathname) {

                if (representsASettingDirectory(pathname)) {

                    if (digestPathname === '.') {
                        digestPathname = path.withoutLowerFloors(2);
                    } else {
                        digestPathname = path.withoutLowerFloors(1);
                    }

                    if (!pathData2Ignore?.pathnamesInDepth?.includes(resolvedAbsolutePathname)) {
                        if (copyIsInDepth(pathname)) {
                            data.pathnamesInDepth?.push(digestPathname);
                        } else {
                            if (!pathData2Ignore?.shallowPathnames?.includes(resolvedAbsolutePathname)) {
                                data.shallowPathnames?.push(digestPathname);
                            }
                        }
                    }

                } else {
                    if (!pathData2Ignore?.filenames?.includes(resolvedAbsolutePathname)) {
                        data.filenames?.push(digestPathname);
                    }
                }

            }

        }

        return data;
    };

    const representsASettingDirectory = (pathname: string) => {

        const path = new Path(pathname);

        if (copyIsInDepth(pathname) || pathname.endsWith('.')) {
            return true;
        }

        return false;
    };

    const resolvePathnames2Ignore = (pathnames: string[]) => {

        const data: PathData = {
            pathnamesInDepth: [],
            shallowPathnames: [],
            filenames: []
        };

        let resolvedAbsolutePathname: string | null;

        let path: Path;

        for (let pathname of pathnames) {

            resolvedAbsolutePathname = resolveAbsolutePathname(absoluteRootPathname, pathname);

            path = new Path(pathname);

            if (resolvedAbsolutePathname) {

                if (representsASettingDirectory(pathname)) {

                    if (copyIsInDepth(pathname)) {
                        data.pathnamesInDepth?.push(resolvedAbsolutePathname);
                    } else {
                        data.shallowPathnames?.push(resolvedAbsolutePathname);
                    }

                } else {
                    data.filenames?.push(resolvedAbsolutePathname);
                }

            }
        }

        return data;
    };

    const resolveAbsolutePathname = (
        absoluteParentPathname: string,
        subpathname: string
    ) => {

        const parentPath = new Path(absoluteParentPathname);

        const path = parentPath.attachNew(subpathname);

        let representsADirectory = representsASettingDirectory(path.toString());

        let digestPathname: string = path.toString();

        let digestPath: Path;

        if (representsADirectory) {
            digestPathname = path.withoutLowerFloors(1);
        }

        if (!NodeFs.existsSync(digestPathname)) {
            if (verbose) {

                digestPath = new Path(digestPathname);

                if (representsADirectory || !digestPath.extension) {
                    console.warn(`No such directory ${digestPathname}\n`);
                } else {
                    console.warn(`No such file ${digestPathname}\n`);
                }
            }

            return null;
        }

        return digestPathname;
    };

    const resolveParentOfPathname = (
        pathname: string,
        settingsFileData: any,
        appendPathname?: string
    ) => {

        let resolvedPathname = Path
            .relativeResolve(Path.join(pathname, settingsFileData.rootPath));

        if (appendPathname) {
            resolvedPathname = Path.join(resolvedPathname, appendPathname);
        }

        return resolvedPathname;
    };

    const loadSettingsFile = (pathname: string) => {

        const jsonFileHandler = new JsonFileHandler(pathname);

        return jsonFileHandler.read();
    };

    const resolveFileSettingsPath = () => {

        /**
         * O process.cwd() retornaria .../node_modules/@cyberjs.on/copfy, isto é, o diretório root da biblioteca em detrimento do diretório root do projeto que usa a biblioteca.
         * Além disso, não haveria possibilidade da flexibilização do caminho onde o arquivo de
         * configuração será encontrado.
         *
         * case insensitive
         */
        const hasConfiguration = Object
            .hasOwn(process.env, 'npm_config_cyberjson_settings_directory');
        // console.log(process.env)

        let buffer: Buffer;

        let settingsPathname: string | undefined;

        let compoundPathname!: string;

        let errorMessage!: string;

        if (hasConfiguration || Object
            .hasOwn(process.env, 'npm_config_cyberjson_settings_directory_from_own_library')) {

            if (hasConfiguration) {
                /**
                 * https://nodejs.dev/pt/learn/how-to-read-environment-variables-from-nodejs/
                 * when use the script through npm explore, it works only by this way
                 */
                settingsPathname = Path.join(process.env['npm_config_cyberjson_settings_directory']!, '@cyberjs.on');
            } else {
                /**
                 * Otherwise the npmrc of the own library overwrites the npmrc of the project.
                 * In this context process.env['npm_config_cyberjson_settings_directory_from_own_library'] stores and processes ${INIT_CWD}\settings literally as value, for this reason, it must do as below
                 */
                buffer = NodeChild_process
                    .execSync('npm get cyberjson_settings_directory_from_own_library');
                settingsPathname = buffer.toString();
            }

            settingsPathname = new Path(settingsPathname!).toString().replace(/\n/g, '');

            if (!NodeFs.existsSync(settingsPathname!)) {
                errorMessage = `The directory \`${settingsPathname}' referenced by environment variable \`cyberjson_settings_directory' doesn't exist.`;
            }

        } else {
            errorMessage = 'The environment variable `cyberjson_settings_directory\' was not defined. See how to do this in https://docs.npmjs.com/cli/v8/configuring-npm/npmrc';
        }

        if (errorMessage) {
            throw new Error(errorMessage);
        }

        compoundPathname = Path.join(settingsPathname!, 'copfy.json');

        return new Path(compoundPathname);
    };

    type PathData = {
        pathnamesInDepth?: string[],
        shallowPathnames?: string[],
        filenames?: string[]
    };

    const operatingSystemPathSeparator = NodePath.sep;

    Path.operatingSystemPathSeparator = operatingSystemPathSeparator;

    const settingsFilePath = resolveFileSettingsPath();

    const settingsFileData: any = await loadSettingsFile(settingsFilePath.toString());

    let settings: any;

    let absoluteRootPathname: string;

    let absoluteRootPath: Path;

    let resolvedCopy2RelativePath: Path;

    let compiledSourcePath: Path;

    let absolutePathOfOrigin: Path;

    let currentAbsolutePath: Path;

    let differencePath: Path;

    let copy2Path: Path;

    let copyFromPath: Path;

    let filenamePaths: Path[];

    let rootDirectory: Directory;

    let shallowDirectory: Directory;

    let inDepthDirectoryIterator: DiMan.Iterators.Directories.Aggregate;

    let copy2Pathname: string;

    let copy2SubpathnameSetting: string | undefined;

    let copyFromPathname: string | undefined;

    let pathnameDifference: string | null;

    let fileExtensionPatterns: string[];

    let currentPathData2Copy: PathData;

    let preCompiledPathnameSettings2Copy: PathData;

    let pathData2Ignore: PathData | null = null;

    let verbose = false;

    let replaceIfAlreadyExists = false;

    settings = settingsFileData.settings;

    verbose = settings.verbose;

    replaceIfAlreadyExists = settings.replaceIfAlreadyExists;

    absoluteRootPathname = resolveParentOfPathname(
        settingsFilePath.directoryname,
        settingsFileData,
        appendPathname
    );
    absoluteRootPath = new Path(absoluteRootPathname);

    compiledSourcePath = new Path(settingsFileData.pathOfCompiledSource);

    rootDirectory = new Directory(absoluteRootPathname);

    if (Object.hasOwn(settings, 'ignoreByPathPatterns') ) {
        pathData2Ignore = resolvePathnames2Ignore(
            settings.ignoreByPathPatterns
        );
        // console.log('pathData2Ignore', pathData2Ignore)
    }

    for (let copySettings of settings.copies) {

        preCompiledPathnameSettings2Copy = preCompilePathnameSettings2Copy(copySettings.paths);
        // console.log('\preCompiledPathnameSettings2Copy', preCompiledPathnameSettings2Copy)

        fileExtensionPatterns = copySettings.byfileExtensionPatterns;

        for (let pathSettings of copySettings.paths) {

            copy2SubpathnameSetting = pathSettings.copy2;

            currentPathData2Copy = resolveCurrentPathPatterns2Copy(pathSettings.patterns);
            // console.log('currentPathData2Copy', currentPathData2Copy)

            /**
             *
             * depth
             */
            if (currentPathData2Copy.pathnamesInDepth?.length) {
                for (let originalSubpathnameSetting
                    of (currentPathData2Copy.pathnamesInDepth)
                ) {

                    resolvedCopy2RelativePath = resolveCopy2Path(
                        originalSubpathnameSetting,
                        copy2SubpathnameSetting
                    );

                    absolutePathOfOrigin = new Path(
                        Path.join(absoluteRootPathname, originalSubpathnameSetting)
                    );

                    inDepthDirectoryIterator = new DiMan.Iterators.Directories
                        .Aggregate(new Directory(absolutePathOfOrigin.toString()));
                    for (let directory of inDepthDirectoryIterator.createIterator()) {

                        currentAbsolutePath = directory!.path;

                        copyFromPathname = currentAbsolutePath.toString();

                        if (
                            (
                                !preCompiledPathnameSettings2Copy.shallowPathnames
                                    ?.includes(copyFromPathname)
                                && !pathData2Ignore?.shallowPathnames?.includes(copyFromPathname)
                                && !pathData2Ignore?.pathnamesInDepth?.includes(copyFromPathname)
                                && !currentAbsolutePath.matchesAsASubpathOfList(
                                    pathData2Ignore?.pathnamesInDepth || []
                                )
                            )
                        ) {

                            copy2Path = absoluteRootPath
                                .attachNew(resolvedCopy2RelativePath.toString());

                            pathnameDifference = absolutePathOfOrigin
                                .differenceFrom(currentAbsolutePath);

                            if (pathnameDifference) {

                                differencePath = new Path(pathnameDifference);

                                if (resolvedCopy2RelativePath) {
                                    differencePath = resolvedCopy2RelativePath
                                        .attachNew(differencePath.toString());
                                }

                                copy2Path = absoluteRootPath.attachNew(differencePath.toString());
                            }

                            copyFiles(
                                directory!.resolveListOfFilePath(),
                                currentAbsolutePath.toString(),
                                copy2Path.toString()
                            );
                        }

                    }

                }
            }

            /**
             *
             * surface
             */
            if (currentPathData2Copy.shallowPathnames?.length) {
                for (let subpathnameSetting of (currentPathData2Copy.shallowPathnames)) {

                    copyFromPathname = Path.join(absoluteRootPathname, subpathnameSetting);

                    shallowDirectory = new Directory(copyFromPathname);

                    copyFromPath = shallowDirectory.path;

                    resolvedCopy2RelativePath = resolveCopy2Path(
                        subpathnameSetting,
                        copy2SubpathnameSetting
                    );

                    copy2Pathname = Path
                        .join(absoluteRootPathname, resolvedCopy2RelativePath.toString());

                    copyFiles(
                        shallowDirectory.resolveListOfFilePath(),
                        copyFromPath.toString(),
                        copy2Pathname
                    );

                }

            }

            /**
             *
             * files
             */
            if (currentPathData2Copy.filenames?.length) {
                filenamePaths = currentPathData2Copy.filenames.map(
                    filePathname => new Path(filePathname)
                );

                for (let path of filenamePaths) {

                    copyFromPathname = Path
                        .join(absoluteRootPathname, path.directoryname);

                    resolvedCopy2RelativePath = resolveCopy2Path(
                        path.directoryname,
                        copy2SubpathnameSetting
                    );

                    copy2Pathname = Path
                        .join(absoluteRootPathname, resolvedCopy2RelativePath.toString());

                    copyFiles(
                        [path.attachNewParent(absoluteRootPathname)],
                        copyFromPathname,
                        copy2Pathname
                    );

                }

            }

        }
    }

}
